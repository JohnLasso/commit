﻿using NBitcoin;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace WalletAcademy
{
    public class RSA
    {
        public static Wallet GenerateKey()
        {
            Key privateKey = new Key();

            var publicKey = privateKey.GetBitcoinSecret(Network.Main).GetAddress();
            var address = BitcoinAddress.Create(publicKey.ToString(), Network.Main);

            return new Wallet { PublicKey = publicKey.ToString(), PrivateKey = privateKey.GetBitcoinSecret(Network.Main).ToString() };
        }

        public static string Sign(string privKey, string msgToSign)
        {
            var secret = Network.Main.CreateBitcoinSecret(privKey);
            var signature = secret.PrivateKey.SignMessage(msgToSign); //var v = secret.PubKey.VerifyMessage(msgToSign, signature);
            return signature;

        }
        public static bool Verify(string pubKey, string originalMessage, string signature)
        {
            var address = new BitcoinPubKeyAddress(pubKey, Network.Main);
            bool verified = address.VerifyMessage(originalMessage, signature);

            return verified;

        }
    }
}
